def odd_occurrences_in_array(a):
    """
    # Given an array consisting integers fulfilling the above conditions
    # Returns the value of the unpaired element

    >>> odd_occurrences_in_array([42, 13, 42])
    13
    >>> odd_occurrences_in_array([0, -13, 42, -13, 7, 42, 0])
    7
    """
    counter = list()
    for i in a:
        if i in counter:
            counter.remove(i)
        else:
            counter.append(i)
    return counter[0]


if __name__ == '__main__':
    import doctest
    doctest.testmod()


# ID: trainingZPRMY3-2DZ
# Correctness 100%
# Performance 25%
# Task score  66%

# Detected time complexity: O(N**2) TODO O(N)
