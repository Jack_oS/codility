# CyclicRotation
# Rotate an array to the right by a given number of steps.


def solution(A, K):
    return A[len(A)-K:] + A[:len(A)-K]
    
# Correctness 90%	Performance 25%	Task score 66%
# FAIL if K>len(A)


def solution_2(A, K):
    if len(A) and K > len(A):
        K %= len(A)
    return A[len(A)-K:] + A[:len(A)-K]
    
# Correctness   100%
# Performance   not assessed
# Task score    100%
# Test score    100%
