# Write a function:
# def solution(A)
# that, given an array A of N integers, returns the smallest positive integer 
# (greater than 0) that does not occur in A.
# For example, given A = [1, 3, 6, 4, 1, 2], the function should return 5.
# Given A = [1, 2, 3], the function should return 4.
# Given A = [−1, −3], the function should return 1.
# Assume that:
# N is an integer within the range [1..100,000];
# each element of array A is an integer within the range [−1,000,000..1,000,000].
# Complexity:
# expected worst-case time complexity is O(N);
# expected worst-case space complexity is O(N), beyond input storage
# (not counting the storage required for input arguments).

def solution(A):
    sorted_list_a = sorted(list(set(A)))
    for i in range(1, 100000):
        if i not in sorted_list_a:
            return i

# Programming language used: Python
# Total time used: 8 minutes
# Effective time used: 8 minutes
# Notes:
# not defined yet

# Analysis summary
# The following issues have been detected: timeout errors.
# 
# Analysis
# Detected time complexity:
# O(N**2)                                                            #TODO O(N)

# Correctness 100%      Performance	25%     Task score 66%