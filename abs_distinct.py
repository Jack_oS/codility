def abs_distinct(a):
    """
    Given a[] non-empty zero-indexed array consisting numbers;
    Returns absolute distinct count of array.
    >>> abs_distinct([])
    0
    >>> abs_distinct([-13, 13])
    1
    >>> abs_distinct([-1, 1])
    1
    >>> abs_distinct([-42, 13, 42])
    2
    >>> abs_distinct([-7, -3, 1, 3, 7])
    3
    >>> abs_distinct([-5, -3, 0, 2, 5, 6])
    5
    """
    abs_set = set()
    for i in a:
        abs_set.add(abs(i))
    return len(abs_set)


if __name__ == '__main__':
    import doctest
    doctest.testmod()


# ID: training5H4R4F-SND
# Correctness 100%
# Performance 100%
# Task score  100%
